//
//  AppDelegate.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 14/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import Firebase
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        
        /*
         Prima di attivare la funzione, assicuratevi che l'utente non abbia già dei valori in quel mese
         Se li ha -> cancellateli da firebase
         
         passate l'uid dell'utente ed un array di mesi che volete ripristinare, al resto pensa la funzione
         
         FIRebaseRestore.init(uid: "cK0AnPwVhSbZHnPWmZ0oOL657LB3").macro_meso(month: [10, 11], year: 2017)
         
         se volete velocizzare l'operazione per più utenti usate questo codice:
         
         var array_id = ["id1", "id2", "id3"]
         
         for x in array_id {
            FIRebaseRestore.init(uid: x).macro_meso(month: [10, 11], year: 2017)
         }
         */
        
        Auth.auth().addStateDidChangeListener { (auth, user) in
            guard let _user = user else {
                print("[Log] You need to Login")
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let loginVC = storyboard.instantiateViewController(withIdentifier: "loginViewController")
                self.window?.rootViewController = loginVC
                return
            }

            print("[Log] Welcome, \(_user.email!)")
            
            Crashlytics.sharedInstance().setUserEmail(_user.email!)
            Crashlytics.sharedInstance().setUserIdentifier(_user.uid)
            
            ColorController.init().retrieve()
                        
            UserController.init().getPreferences(uid: _user.uid, completionHandler: { () in
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "homeViewController")
                self.window?.rootViewController = homeVC
            })
            
        }
        
            
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let loadingVC = storyboard.instantiateViewController(withIdentifier: "loadingViewController")
        self.window?.rootViewController = loadingVC
 
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }



}


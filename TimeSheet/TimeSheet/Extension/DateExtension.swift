//
//  Date.swift
//  Your School Diary
//
//  Created by Giuseppe Sapienza on 11/08/17.
//  Copyright © 2017 Giuseppe Sapienza. All rights reserved.
//

import UIKit

extension Date {
    
    init(string: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self = dateFormatter.date(from: string) ?? Date.init()
    }

    var _string: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: self)
    }
    
    var dayNumber: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        return dateFormatter.string(from: self).capitalized
    }
    
    var dayName: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
    
    var hour: String {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: self)
    }
    
    var monthName: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.dateFormat = "LLLL"
        return dateFormatter.string(from: self).capitalized
    }
    
    var year: String {
        return "\(Calendar.current.dateComponents([.year], from: self).year ?? 1999)"
    }
    
    var next: Date? {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)
    }
    
    var prev: Date? {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)
    }
    
    var monthDateInterval: DateInterval {
        return Calendar.current.dateInterval(of: .month, for: self)!
    }
    
    func dayAfter(_ i: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: i, to: self) ?? self
    }
    
    func dayBefore(_ i: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: -i, to: self) ?? self
    }
    
    var week: Week {
        return Week.init(date: self, calendar: .current)
    }
    
    struct Week {
        var fromDate: Date
        var calendar: Calendar
        
        var numberOfMonth: Int? {
            return self.calendar.dateComponents([.weekOfMonth], from: self.fromDate).weekOfMonth
        }
        
        var month: Int? {
            return self.calendar.dateComponents([.month], from: self.fromDate).month
        }
        
        var year: Int? {
            return self.calendar.dateComponents([.yearForWeekOfYear], from: self.fromDate).yearForWeekOfYear
        }
        
        var number: Int? {
            return self.calendar.dateComponents([.weekOfYear], from: self.fromDate).weekOfYear
        }
        
        var start: Date? {
            return self.calendar.date(from: self.calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self.fromDate))
        }
        
        var end: Date? {
            guard let _start = start else { return nil }
            return self.calendar.date(byAdding: .day, value: 7, to: _start)
        }
        
        init(date: Date, calendar: Calendar = .current) {
            self.fromDate = date
            self.calendar = calendar
        }
    }
    
}


//
//  AdminViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 30/08/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class AdminViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var AdminTableView: UITableView!
    @IBOutlet weak var CollaboratoriSearchBar: UISearchBar!
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    @IBOutlet var chartsButton: CircleButton!
    @IBOutlet var timeSheetButton: CircleButton!
    
    let tableViewController = AdminTableViewController.init()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.CollaboratoriSearchBar.delegate = self
        
        switch CurrentUser.shared.role {
        case "Admin":
            UsersController.init().download { (users) in
                self.showUsers(array: users)
            }
            
        case "Responsabile":
            ResponsabileController.init().retrieve(completionHandler: { (users) in
                self.showUsers(array: users)
            })
            
        default: return
        }
        
        self.animStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func showUsers(array users: [User]) {
        let orderdUsers = users.sorted(by: { (a, b) -> Bool in
            return a.name < b.name
        })
        
        self.tableViewController.datasource = orderdUsers
        self.tableViewController.dataSourceFiltered = orderdUsers
        self.tableViewController.viewcontroller = self
        self.AdminTableView.dataSource = self.tableViewController
        self.AdminTableView.delegate = self.tableViewController
        self.AdminTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.tableViewController.dataSourceFiltered = self.tableViewController.datasource
            self.AdminTableView.reloadData()
            return
        }
        
        let arrayFiltrato = tableViewController.datasource.filter { (orderdUsers) -> Bool in
            return orderdUsers.name.lowercased().hasPrefix(searchText.lowercased())
        }
        
        guard arrayFiltrato.count != 0 else {
            self.tableViewController.dataSourceFiltered = self.tableViewController.datasource
            self.AdminTableView.reloadData()
            return
        }
        
        self.tableViewController.dataSourceFiltered = arrayFiltrato
        self.AdminTableView.reloadData()
    }
    
    func animStart() {
        self.chartsButton.transform = CGAffineTransform.init(translationX: 0, y: 200)
        self.timeSheetButton.transform = CGAffineTransform.init(translationX: 0, y: 200)
    }
    
    func animShowButtons() {
        UIView.animate(withDuration: 0.5, animations: {
            self.chartsButton.transform = CGAffineTransform.identity
            self.timeSheetButton.transform = CGAffineTransform.identity
        }) { (_) in
            self.chartsButton.transform = CGAffineTransform.init(scaleX: 0.3, y: 0.3)
            self.timeSheetButton.transform = CGAffineTransform.init(scaleX: 0.3, y: 0.3)
            
            UIView.animate(withDuration: 0.2, animations: {
                self.chartsButton.transform = CGAffineTransform.identity
                self.timeSheetButton.transform = CGAffineTransform.identity
            })
            
        }

    }

    
    @IBAction func timesheetButton_clicked(_ sender: Any) {
        guard let id = self.tableViewController.selectedUID else {
            return
        }
       let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TimeBlocksViewController") as! TimeBlocksViewController
        vc.selectedUID = id
        
        self.tabBarController?.tabBar.isHidden = true
        self.show(vc, sender: nil)
        
    }
    
    @IBAction func graficiButton_clicked(_ sender: Any) {
        guard let id = self.tableViewController.selectedUID else {
            return
        }
       let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BarChartViewController") as! BarChartViewController
        vc.selectedUID = id
        
        self.tabBarController?.tabBar.isHidden = true
        self.show(vc, sender: nil)
    }
}

//
//  FerieViewController.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 04/09/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class FerieViewController: UIViewController {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func confermaButton_Clicked(_ sender: Any) {
        
        let type = self.segmentedControl.titleForSegment(at: self.segmentedControl.selectedSegmentIndex)
        
        HolidayController.init(date: self.datePicker.date).add(type: type!)
        
        let alert = UIAlertController.init(title: "FERIE AGGIUNTA", message: "Vuoi aggiungerne un'altra?", preferredStyle: .alert)
        
        let yesButton = UIAlertAction.init(title: "SI", style: .default, handler: nil)
        let closeButton = UIAlertAction.init(title: "NO", style: .destructive, handler: { _ in
            self.navigationController?.popViewController(animated: true)
        })
        
        alert.addAction(yesButton)
        alert.addAction(closeButton)
        
        self.present(alert, animated: true, completion: nil)
        
    }

}

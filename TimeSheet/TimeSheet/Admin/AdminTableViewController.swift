//
//  AdminTableViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 30/08/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import Firebase

class AdminTableViewController: NSObject {

    weak var viewcontroller : AdminViewController!
    
    var selectedUID: String?
    
    var datasource : [User] = []
    var dataSourceFiltered : [User] = []
    var isFiltered : Bool = false
    
    override init() {
        
    }
}



extension AdminTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceFiltered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AdminTableViewCell
        
        cell.usernameLabel.text = self.dataSourceFiltered[indexPath.row].name
        
        return cell
    }
    
}

extension AdminTableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedUID = self.dataSourceFiltered[indexPath.row].id
        self.viewcontroller.animShowButtons()
    }
    

}



//
//  AdminTableViewCell.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 30/08/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class AdminTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

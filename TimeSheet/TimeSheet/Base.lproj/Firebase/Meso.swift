//
//  Meso.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 16/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct MesoController {
    
    func retrieve(completionHandler: @escaping ([Meso])->()) {
        
        Database.database().reference().child("Meso").observeSingleEvent(of: .value, with: { (snap) in
            guard let value = snap.value as? [String:String] else { return }
            
            var meso: [Meso] = []
            
            for x in value {
                let name = x.key
                meso.append(Meso.init(name: name))
            }
            
            
            completionHandler(meso)
            
        })
        
}
}

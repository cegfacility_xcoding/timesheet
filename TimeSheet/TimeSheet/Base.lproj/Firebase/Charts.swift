//
//  Charts.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 26/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

struct MacroChart {
    var name: String
    var value: Double
}

struct GroupChart {
    var id: String
    var array: [MacroChart]
}

struct ChartsFirebase {

    let components = Calendar.current.dateComponents([.day, .month, .year], from: Date.init())
    
    var selectedUID: String
    
    init(uid: String) {
        self.selectedUID = uid
    }

    func macro(year: Int, month: Int, completionHandler: @escaping ([GroupChart])->()) {
        
        Database.database().reference().child("Calcolo-Macro").child(self.selectedUID).child("\(year)/\(month)").observeSingleEvent(of: .value, with: {snap in
            guard let dict = snap.value as? [String:[String:Double]] else { return }
            
            var groupArray: [GroupChart] = []
            
            for (key, macroDict) in dict {
                
                var macroCharts: [MacroChart] = []
                
                for (name, value) in macroDict {
                    let macroChart = MacroChart.init(name: name, value: value)
                    macroCharts.append(macroChart)
                }
                
                let group = GroupChart.init(id: key, array: macroCharts)
                groupArray.append(group)
            }
            
            completionHandler(groupArray)
        })
    }
    
    
    func meso(year: Int, month: Int, completionHandler: @escaping ([String:Double])->()) {
       Database.database().reference().child("Calcolo-Meso").child(self.selectedUID).child("\(year)/\(month)").observeSingleEvent(of: .value, with: {snap in
            guard let dict = snap.value as? [String:Double] else { return }
            completionHandler(dict)
       })
    }
    
    func luogo(year: Int, month: Int, completionHandler: @escaping ([String:Double])->()) {
        Database.database().reference().child("Calcolo-InOut").child(self.selectedUID).child("\(year)/\(month)").observeSingleEvent(of: .value, with: {snap in
            guard let dict = snap.value as? [String:Double] else { return }
            completionHandler(dict)
        })
    }

}

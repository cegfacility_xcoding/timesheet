//
//  Restore.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 14/12/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import FirebaseDatabase

class FIRebaseRestore {
    
    init(uid: String) {
        self.uid = uid
    }
    
    var uid: String
    
    func macro_meso(month: [Int], year: Int) {
        for x in month {
            self.activities_of(month: x, year: year) { (userAct, act) in
                let components = Calendar.current.dateComponents([.day, .month, .year], from: act.giorno)
                userAct.increment_Meso(components, act)
                userAct.increment_Macro(components, act)
            }
        }
    }
    
    fileprivate func activities_of(month: Int, year: Int, completionHandler: @escaping (UserActivity, Attività)->()) {
        let date = Date.init(string: "01/\(month)/\(year)")
        let a = date.monthDateInterval.start
        let b = date.monthDateInterval.end.prev!
        
        for i in Int(a.dayNumber)!...Int(b.dayNumber)! {
            let date = Date.init(string: "\(i)/\(month)/\(year)")
            let userAct = UserActivity.init(uid: self.uid, date: date)
            userAct.download(completionHandler: { (block, down) in
                let act = Attività.init(giorno: date, block: block, luogo: down.luogo!, macro: down.macro!, meso: down.meso!, note: down.note!)
                completionHandler(userAct,act)
            })
        }
    }

    
}

//
//  UsersController.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 28/08/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct UsersController {

    func download(completionHandler: @escaping ([User])->()) {
        var users: [User] = []
        Database.database().reference().child("Utenti").observe(.value, with: { (snap) in
            guard let value = snap.value as? [String:Any] else{return}
            for user in value{
                let id = user.key
                guard let dati =  user.value as? [String:String] else {return}
                let nome = dati["name"]
                
                let newUser = User.init(id: id, name: nome!)
                users.append(newUser)
            }
            
            completionHandler(users)
        })
        
        
    
    }
}

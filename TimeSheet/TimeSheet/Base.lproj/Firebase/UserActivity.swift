//
//  UserActivity.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 21/09/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import FirebaseDatabase

struct UserActivity {

    let ref = Database.database().reference()
    
    let uid: String
    let date: Date
    
    var userActivityRef: DatabaseReference {
        let components = Calendar.current.dateComponents([.day, .month, .year], from: self.date)
        return ref.child("Calendario-Attività/\(self.uid)/\(components.year!)/\(components.month!)/\(components.day!)")
    }
    
    init(uid: String = CurrentUser.shared.id, date: Date) {
        self.uid = uid
        self.date = date
    }
    
    func add(attività: Attività) {
        let blocco = attività.block
        
        let payload: [String:Any] = [
            "luogo" : attività.luogo,
            "macro" : [
                "id" : attività.macro.id,
                "name" : attività.macro.name
            ],
            "meso" : attività.meso.name,
            "note" : attività.note
        ]
        
        self.incrementActivityCount(attività)
        
        self.userActivityRef.child(blocco).setValue(payload)
        print("[Log] Activity Added")
    }
    
    // meno -0.5 dal 
    func remove(attività: DownloadedActivity) {
        let components = Calendar.current.dateComponents([.day, .month, .year], from: self.date)
        
        let refMacro = Database.database().reference().child("Calcolo-Macro").child(self.uid).child("\(components.year!)/\(components.month!)").child(attività.macro!.id).child(attività.macro!.name)
        
        refMacro.observeSingleEvent(of: .value, with: { snap in
            
            guard let pre = snap.value as? Double else { return }
            guard pre != 0.5 else {
                refMacro.setValue(nil)
                return
            }
            refMacro.setValue(pre - 0.5)
            
        })
        
        let refMeso = Database.database().reference().child("Calcolo-Meso").child(self.uid).child("\(components.year!)/\(components.month!)").child(attività.meso!.name)
        
        refMeso.observeSingleEvent(of: .value,with: { snap in
        
            guard let pre = snap.value as? Double else {return}
            guard pre != 0.5 else {
                refMacro.setValue(nil)
                return
            }
            refMeso.setValue(pre - 0.5)
            
        })
        
        let refInOut = Database.database().reference().child("Calcolo-InOut").child(self.uid).child("\(components.year!)/\(components.month!)").child(attività.luogo!)
        
        refInOut.observeSingleEvent(of: .value,with: { snap in
            
            guard let pre = snap.value as? Double else {return}
            guard pre != 0.5 else {
                refMacro.setValue(nil)
                return
            }
            refInOut.setValue(pre - 0.5)
        })
        
        print("[Log] Activity Removed")
    }
    
    
    func download(completionHandler: @escaping (String, DownloadedActivity) -> ()) {
        let components = Calendar.current.dateComponents([.day, .month, .year], from: self.date)
        
        let path = "Calendario-Attività/\(self.uid)/\(components.year!)/\(components.month!)/\(components.day!)"
        let selectedDayRef = self.ref.child(path)
        
        selectedDayRef.observe(.childAdded, with: { (snap) in
            let block = snap.key
            guard let dict = snap.value as? [String:Any] else { return }
            
            guard
                let macro = dict["macro"] as? [String : String],
                let macroID = macro["id"],
                let macroName = macro["name"],
                let meso = dict["meso"] as? String,
                let note = dict["note"] as? String,
                let luogo = dict["luogo"] as? String
            else { return }
            
            let activity = DownloadedActivity.init(ref: path + "/" + snap.key, macro: Macro(id: macroID, name: macroName), meso: Meso(name: meso), note: note, luogo: luogo)
            completionHandler(block, activity)
            
        })
    }
    
    func incrementActivityCount(_ attività: Attività) {
        let components = Calendar.current.dateComponents([.day, .month, .year], from: attività.giorno)
        self.increment_Macro(components, attività)
        self.increment_Meso(components, attività)
        self.increment_InOut(components, attività)
    }
    
    
    func increment_InOut(_ components: DateComponents, _ attività: Attività) {
        let refInOut = Database.database().reference().child("Calcolo-InOut").child(self.uid).child("\(components.year!)/\(components.month!)").child(attività.luogo)
        refInOut.observeSingleEvent(of: .value,with: { snap in
            guard snap.exists() else{
                refInOut.setValue(0.5)
                return
            }
            guard let pre = snap.value as? Double else {return}
            
            refInOut.setValue(pre + 0.5)
        })
    }
    
    func increment_Meso(_ components: DateComponents, _ attività: Attività) {
        let refMeso = Database.database().reference()
            .child("Calcolo-Meso").child(self.uid)
            .child("\(components.year!)/\(components.month!)")
            .child(attività.meso.name)
        
        refMeso.runTransactionBlock({ (data) -> TransactionResult in
            guard let value = data.value as? Double else {
                data.value = 0.5
                return TransactionResult.success(withValue: data)
            }
            
            data.value = value + 0.5
            return TransactionResult.success(withValue: data)
        })
    }
    
    func increment_Macro(_ components: DateComponents, _ attività: Attività) {
        let refMacro = Database.database().reference().child("Calcolo-Macro").child(self.uid).child("\(components.year!)/\(components.month!)").child(attività.macro.id).child(attività.macro.name)
        
        refMacro.runTransactionBlock({ (data) -> TransactionResult in
            guard let value = data.value as? Double else {
                data.value = 0.5
                return TransactionResult.success(withValue: data)
            }
            
            data.value = value + 0.5
            return TransactionResult.success(withValue: data)
        })
    }



}

//
//  MacroController.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 16/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct MacroController {
    
    func retrieve(completionHandler: @escaping ([Macro])->()) {

    Database.database().reference().child("Macro").observe(.value, with: { (snap) in
        guard let value = snap.value as? [String:Any] else { return }
            var macro: [Macro] = []
            
            for macrocat in value {
                guard let dati = macrocat.value as? [String:Any] else {return}
                let excludedMacro = CurrentUser.shared.excludedMacro
                for singlemacro in dati {
                    let name = singlemacro.key
                    if excludedMacro.contains(name) { continue }
                    macro.append(Macro.init(id: macrocat.key, name: name))
                }
            }
            
            print("[Log] Macro OK")
            completionHandler(macro)
            
        })
        
    }
}

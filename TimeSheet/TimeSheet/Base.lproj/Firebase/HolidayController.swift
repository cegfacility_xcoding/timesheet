//
//  FerieController.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 04/09/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//
import UIKit
import FirebaseAuth
import FirebaseDatabase


class HolidayController {
    
    init(date: Date) {
        self.date = date
    }
    
    let ref = Database.database().reference()
    let date: Date
    
    func add(type: String) {

        let components = Calendar.current.dateComponents([.day, .month, .year], from: self.date)
        
        ref.child("CalendarioFerie/date/\(components.year!)/\(components.month!)/day-\(components.day!)").setValue(type)
        print("[Log] Holiday Added")
    }
    
    func download(completionHandler: @escaping ([Holiday]) -> ()) {
        let ref = Database.database().reference()
        
        ref.child("CalendarioFerie/date/\(self.date.year)").observeSingleEvent(of: .value, with: {snap in
            guard let dict = snap.value as? [String: [String:String]] else { return }
            
            var holidays: [Holiday] = []
            for (month, daysDict) in dict {
                for (day, type) in daysDict {
                    let formatter = DateFormatter.init()
                    formatter.timeZone = TimeZone.init(identifier: "GMT+0:00")
                    formatter.dateFormat = "dd/MM/yyyy"
                    let dayNumber = day.components(separatedBy: "-")[1]
                    let date = formatter.date(from: "\(dayNumber)/\(month)/2017")!
                    let holiday = Holiday.init(date: date, type: type)
                    holidays.append(holiday)
                }
            }
            
            completionHandler(holidays)
        })
    }
    
}

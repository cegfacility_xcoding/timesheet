//
//  UserController.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 29/08/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import FirebaseAuth
import FirebaseDatabase

struct UserController {
    func login(email: String, password: String, completionHandler: @escaping ()->(), errorHandler: @escaping ()->()) {
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let _ = error {
                print("[Log] SignIn Error:", error!)
                errorHandler()
                return
            }
        }
    
    
    }
    
    func getPreferences(uid: String, completionHandler: @escaping ()->()) {
        let ref = Database.database().reference()
        let utenti = ref.child("Utenti")
        let userChild = utenti.child(uid)

        userChild.observeSingleEvent(of: .value, with: { (snap) in
            guard
                let userData = snap.value as? [String:String],
                let group = userData["gruppo"],
                let role = userData["role"]
            else { return }
            
            CurrentUser.shared.id = uid
            CurrentUser.shared.group = group
            CurrentUser.shared.role = role
            
            completionHandler()
            
            /*
            self.getExcludedMacro(group: group, completionHandler: {
                self.getExcludedMeso(group: group, completionHandler: {
                    print("[Log] Preferences OK")
                    completionHandler()
                })
            }) */
        })
    }
    
    fileprivate func getExcludedMacro(group: String, completionHandler: @escaping () -> ()) {
        Database.database().reference().child("Permessi/\(group)").observeSingleEvent(of: .value, with: { (snap) in
            guard
                let diz = snap.value as? [String:[String:String]],
                let excludedMacro = diz["Macro"]?["Escludi"]
            else {
                completionHandler()
                return
            }
            
            CurrentUser.shared.excludedMacro = excludedMacro
            
            completionHandler()
        })
    }
    
    fileprivate func getExcludedMeso(group: String, completionHandler: @escaping () -> ()) {
        Database.database().reference().child("Permessi/\(group)").observeSingleEvent(of: .value, with: { (snap) in
            guard
                let diz = snap.value as? [String:[String:String]],
                let excludedMeso = diz["Meso"]?["Escludi"]
            else {
                completionHandler()
                return
            }
            
            CurrentUser.shared.excludedMeso = excludedMeso
            
            completionHandler()
        })
    }
    
}

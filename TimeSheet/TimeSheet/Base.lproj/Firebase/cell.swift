

//
//  MacroController.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 16/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct CellFB {
    
    func retrieve(date: Date, completionHandler: @escaping (_ date: Date, _ value: [String])->(), errorHandler: @escaping () -> ()) {
        let ref = Database.database().reference()
        
        let components = Calendar.current.dateComponents([.day, .month, .year], from: date)
        
        ref.child("calendario/\(components.year!)/\(components.month!)/\(components.day!)").observeSingleEvent(of: .value, with: { (snap) in
            
            guard let value = snap.value as? [String:Any] else {
                errorHandler()
                return
            }
            
            var cellfb : [String] = []
            for x in value{
                cellfb.append(x.key)
            }
            
            completionHandler(date, cellfb)

        })
        
            
    }
}


//
//  ResponsabileController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 05/10/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import Foundation
import Firebase

struct ResponsabileController {
    func retrieve(completionHandler: @escaping ([User])->()){
        
        Database.database().reference().child("Responsabili").child(CurrentUser.shared.id).observeSingleEvent(of: .value, with: { (snap) in
            guard let dict = snap.value as? [String:String] else { return }
            
            var array: [User] = []
            
            for (key, name) in dict {
                array.append(User.init(id: key, name: name))
            }
            
            completionHandler(array)
        })
    }
    
}


//
//  ColorController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 04/10/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct ColorController {
    func retrieve() {
        Database.database().reference().child("Macro-Color").observeSingleEvent(of: .value , with: { (snap) in
            guard let dict = snap.value as? [String:String] else { return }
            
            for (key, colorHex) in dict {
                let color = UIColor.init(hex: colorHex) 
                MacroColor.array.append(MacroColor.init(name: key, color: color))
            }
            
        })
    }
}

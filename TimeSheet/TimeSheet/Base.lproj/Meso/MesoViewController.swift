//
//  MesoViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 15/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class MesoViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var MesoSearchBar: UISearchBar!
    @IBOutlet weak var MesoTableView: UITableView!
    
    @IBOutlet weak var macroLabel: UILabel!
    
    var selectedMacro: Macro?
    var selectedDate: Date?
    var selectedblock: BlockView?
    
    let tableViewController = MesoTableViewController.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewController.viewcontroller = self
        self.MesoSearchBar.delegate = self
        self.MesoTableView.dataSource = self.tableViewController
        self.MesoTableView.delegate = self.tableViewController
        
        self.macroLabel.text = self.selectedMacro?.name
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "segueToNote":
            let vc = segue.destination as! NoteViewController
            
            vc.selectedDate = self.selectedDate
            vc.selectedMacro = self.selectedMacro
            vc.selectedblock = self.selectedblock
            vc.selectedmeso = sender as? Meso
            
        default: return
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            tableViewController.isFiltered = false
            self.MesoTableView.reloadData()
            return
        }
        
        tableViewController.isFiltered = true
        
        let arrayFiltrato = tableViewController.dataSource.filter { (meso) -> Bool in
            return meso.name.lowercased().hasPrefix(searchText.lowercased())
        }
        
        tableViewController.filteredDataSource = arrayFiltrato
        self.MesoTableView.reloadData()
        
        
    }

    

}

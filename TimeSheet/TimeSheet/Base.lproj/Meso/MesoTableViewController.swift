//
//  MacroTableViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 15/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class MesoTableViewController: NSObject {
    
    
    
    var viewcontroller : MesoViewController!
    
    var dataSource : [Meso] = []
    
    var isFiltered: Bool = false
    
    
    var filteredDataSource: [Meso] = []
    
        
    override init() {
        super.init()
        
        MesoController.init().retrieve { (meso) in
            self.dataSource = meso
            self.viewcontroller.MesoTableView.reloadData()
        }
    }
    
    var dataSourceSorted: [Meso] {
        get {
            let tempArray = isFiltered ? filteredDataSource : dataSource
            
            return tempArray.sorted { (a, b) -> Bool in
                if a.name > b.name {
                    return false
                } else {
                    return true
                }
            }
            
        }
    }
}
extension MesoTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceSorted.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let meso = self.dataSourceSorted[indexPath.row]
        cell.textLabel?.text = meso.name
        
        cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        if let activity = self.viewcontroller.selectedblock?.activity, activity.meso!.name ==  meso.name {
            cell.backgroundColor = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
        }
        
        return cell
    }
    
}

extension MesoTableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let meso = self.dataSourceSorted[indexPath.row]
        self.viewcontroller.performSegue(withIdentifier: "segueToNote", sender: meso)
    }
    
}

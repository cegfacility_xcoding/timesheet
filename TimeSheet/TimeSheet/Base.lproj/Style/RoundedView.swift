//
//  RoundedView.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 20/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class RoundedView: UIView {
    
    let borderColor: CGColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor
    let roundedValue: CGFloat = 10
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    
    func setup() {
        self.layer.borderColor = self.borderColor
        self.layer.cornerRadius = self.roundedValue
        self.layer.borderWidth = 1
    }
    
    
    
}

class CircleImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    func setup() {
        self.layer.cornerRadius = self.frame.width/2
    }
}

class CircleButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    func setup() {
        self.layer.cornerRadius = self.frame.width/2
    }
    
}

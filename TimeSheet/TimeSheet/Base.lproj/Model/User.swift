
//
//  User.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 17/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class User {
    var id: String
    var name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

class CurrentUser: User {
    static let shared = CurrentUser.init()
    
    var group: String
    var role: String = ""
    
    var excludedMacro: String
    var excludedMeso: String
    
    
    private init() {
        self.group = ""
        self.excludedMacro = ""
        self.excludedMeso = ""
        super.init(id: "", name: "")
    }

    
}

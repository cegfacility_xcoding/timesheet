//
//  Color.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 04/10/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

struct MacroColor {
    static public var array: [MacroColor] = []
    var name: String
    var color: UIColor

    init(name: String, color: UIColor) {
        self.name = name
        self.color = color
    }
}

//
//  Macro.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 16/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

struct Macro {
    var id: String
    var name: String
    
}

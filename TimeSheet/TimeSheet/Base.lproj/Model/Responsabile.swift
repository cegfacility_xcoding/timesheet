//
//  Responsabile.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 04/10/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

struct Responsabile {
    var id: String
    var user: [User]
    
    init(id: String, user: [User]) {
        self.id = id
        self.user = user
    }
}

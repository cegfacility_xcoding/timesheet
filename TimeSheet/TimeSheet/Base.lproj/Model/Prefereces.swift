//
//  Prefereces.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 19/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import Foundation

class Preferences {
    static let shared = Preferences.init()
    
    var accountType: String = ""
    
    var userId: String = ""
   
    private init() {}
}

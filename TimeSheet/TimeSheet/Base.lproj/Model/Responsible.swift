//
//  Responsible.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 04/10/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

struct Responsible{
    var id : String
    var users : [String : String]
    
    init(id : String, user : [String : String]) {
        self.id = id
        self.users = user
    }
}

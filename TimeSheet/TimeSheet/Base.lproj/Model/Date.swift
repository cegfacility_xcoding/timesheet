//
//  data.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 20/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

extension Date {

    var string: String {
        let giorno = Calendar.current.component(.day, from: self)
        let mese = Calendar.current.component(.month, from: self)
        let anno = Calendar.current.component(.year, from: self)
        
        var today = ""
        
        if(mese < 10) {
            today = "\(giorno)-0\(mese)-\(anno)"
        }
        else{
            today = "\(giorno)-\(mese)-\(anno)"
        }
        
        return today
    }
    
}

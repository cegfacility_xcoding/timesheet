//
//  Attività.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 17/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

struct DownloadedActivity {
    
    var ref: String? 
    var macro: Macro?
    var meso: Meso?
    var note: String?
    var luogo: String?
}


struct Attività { 
    
    var giorno: Date
    var block: String
    
    var luogo: String
    var macro: Macro
    var meso: Meso
    var note: String
    
    
    init(giorno: Date, block: String, luogo: String, macro: Macro, meso: Meso, note: String) {
        self.giorno = giorno
        self.block = block
        self.luogo = luogo
        self.macro = macro
        self.meso = meso
        self.note = note
    }
    
    init() { self.init(giorno: Date.init(), block: "", luogo: "", macro: Macro.init(id: "", name: ""), meso: Meso.init(name: ""), note: "") }
}


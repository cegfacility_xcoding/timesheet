//
//  Holiday.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 04/09/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

struct Holiday {
    var date: Date
    var type: String
}

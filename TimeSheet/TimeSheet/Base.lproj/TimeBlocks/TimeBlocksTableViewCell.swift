//
//  TimeBlocksTableViewswift
//  TimeSheet
//
//  Created by Patrick Lucà on 16/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class TimeBlocksTableViewCell: UITableViewCell {
    
    //var viewcontroller : TimeBlocksViewController!

    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var mesoLabel: UILabel!
    @IBOutlet weak var macroLabel: UILabel!
    @IBOutlet weak var LabelCell: UILabel!
    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var inoutImage: UIImageView!
 
    func reset() {
        macroLabel.text = nil
        mesoLabel.text = nil
        descriptionTextView.text = nil
        barView.backgroundColor = nil
    }
    
    func setBarColor(activity: String) {
        switch activity {
        case "Cdd", "Dashboard", "Dg":
            barView.backgroundColor = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)
            
        case "Kam", "Bo", "Qa Hs", "Qa Ss", "Amm", "Hr", "Cpo", "It", "S&C", "Sic","Sviluppo It","Bd","Segreteria", "Sviluppo Commerciale":
            barView.backgroundColor = #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1)
            
        case "App Ceg", "Ceg Bit", "Casa Ceg", "Iot", "Smart Working", "Swiss", "Tribù", "Valutazione Risorse", "B2C":
            barView.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
            
        case "Manutentori", "Ancillari", "Pulizie":
            barView.backgroundColor = #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1)
            
        case "Adidas", "Ala 97", "At&T", "Bolton", "Branca", "Capgemini", "Cattolica", "Cbre - Merrill Lynch", "Ceresio7", "Comdata", "Con Te", "De Agostini", "Dnv", "Dove Vivo", "Dsquared", "Econocom", "Gamestop", "Hilton Hotel", "Ibis Hotel", "Iol", "Janssen", "Montenegro","Mail Barret", "Pall", "Samsung", "Santander", "Schindler", "Thermofisher", "Wki", "Altri Clienti":
            barView.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
            
        case "Ceg Easy Friday", "Ferie", "Legge 104", "Permesso", "Malattia", "Infortunio":
            barView.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
            
        default: return
        }
    
    }
    


}

//
//  CustomCell.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 19/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCell: JTAppleCell {
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var selectedView : UIView!
   
}

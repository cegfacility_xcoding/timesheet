//
//  TimeBlocksViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 15/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import JTAppleCalendar
import FirebaseDatabase

class TimeBlocksViewController: UIViewController {
    let formatter = DateFormatter()
    
    @IBOutlet var calendarView: JTAppleCalendarView!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var monthLabel: UILabel!

    @IBOutlet var holidayView: UIView!
    @IBOutlet var holidayLabel: UILabel!
    @IBOutlet var holidayHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var hourLabel: UILabel!
    
    @IBOutlet var TimeBlocksTableView: UITableView!
    
    let outsideMonthColor = UIColor(colorWithHexValue : 0x584a66)
    let monthColor = UIColor(colorWithHexValue : 0x314000)
    let selectedMonthColor = UIColor.white
    let currentDateSelectedViewColor = UIColor(colorWithHexValue : 0x4e3f5d)
    
    var tableViewController : TimeBlocksTableViewController!
    
    var selectedUID: String = CurrentUser.shared.id

    var holidays: [Holiday] = []
    
    var dateSelected: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewController = TimeBlocksTableViewController.init()
        self.tableViewController.selectedUID = self.selectedUID
        
        self.calendarView.scrollToDate(Date(), animateScroll: false)
        
        self.setupCalendarView()
        self.tableViewController.viewcontroller = self
        
        self.TimeBlocksTableView.dataSource = self.tableViewController
        self.TimeBlocksTableView.delegate = self.tableViewController
        
        self.holidayHeightConstraint.constant = 0
        
        let today = Date.init()
        
        HolidayController.init(date: today).download(completionHandler: { holidays in
            self.holidays = holidays
            self.isHoliday(date: today)
        })
    
        self.scrollToHour()
        
        self.tableViewController.downloadActivities(selectedDay: Date.init())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        guard let _ = self.dateSelected else { return }
        
        HolidayController.init(date: self.dateSelected!).download(completionHandler: { holidays in
            self.holidays = holidays
        })
        
        self.calendarView.selectDates([self.dateSelected!])
    }
    
    func scrollToHour(_ hour: String = "09:00") {
        guard let i = self.tableViewController.datasource.index(where: { (block) -> Bool in
            if block.time == hour { return true }
            return false
        }) else { return }
        
        self.TimeBlocksTableView.scrollToRow(at: IndexPath.init(row: i, section: 0), at: .top, animated: true)
    }

    
    func isHoliday(date: Date) {
        let holiday = self.holidays.first { (h) -> Bool in
            if h.date.string == date.string { return true }
            return false
        }
        
        self.holidayView.isHidden = true
        
        if holiday == nil {
            self.holidayHeightConstraint.constant = 0
            return
        }
        
        self.holidayView.isHidden = false
        
        self.holidayHeightConstraint.constant = 44
        self.holidayLabel.text = holiday!.type
    }
    
    
    // Setup the margin of the cell
    func setupCalendarView(){
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        
        //setup Labels
        calendarView.visibleDates { (visibleDates) in
            self.setupViewOfCalendar(from: visibleDates)
        }
    }
    
    func handleCelltextColor(view: JTAppleCell?, cellState: CellState){
        guard let validCell = view as? CustomCell else { return }
        
        switch cellState.isSelected {
        case true:
            validCell.dateLabel.textColor = self.selectedMonthColor
            
        case false:
            validCell.dateLabel.textColor = self.outsideMonthColor
            
            if cellState.date.string == Date.init().string {
                validCell.dateLabel.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            }
        }
        
    }
    
    func handleCellSelected(view: JTAppleCell?, cellState: CellState) {
        guard let validCell = view as? CustomCell else { return }
        
        switch cellState.isSelected {
        case true:
            validCell.selectedView.isHidden = false
            self.isHoliday(date: cellState.date)
            
        case false:
            validCell.selectedView.isHidden = true
        }
        

    }
    
    func setupViewOfCalendar(from visibleDates : DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        self.yearLabel.text = date.year
        self.monthLabel.text = date.monthName
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "segueToMacro":
            let vc = segue.destination as! MacroViewController
            vc.selectedDate = self.dateSelected ?? Date.init()
            vc.selectedblock = sender as? BlockView
            
        default: return
        }
        
    }


}

// COnfigurazione del calendario
extension TimeBlocksViewController :  JTAppleCalendarViewDataSource {
    
   
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from: "2017 01 01")!
        let endDate = formatter.date(from: "2018 12 31")!
        
        let paramters = ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 1, calendar: Calendar.current, generateInDates: .forAllMonths, generateOutDates: .tillEndOfRow, firstDayOfWeek: .sunday, hasStrictBoundaries: true)
        return paramters
    }
}

extension TimeBlocksViewController : JTAppleCalendarViewDelegate{
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        
        cell.dateLabel.text = cellState.text
       
        handleCellSelected(view: cell, cellState: cellState)
        handleCelltextColor(view: cell, cellState: cellState)
        
        if cellState.date.string == Date.init().string {
            cell.dateLabel.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        }
        
        return cell
    }

    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        self.handleCellSelected(view: cell, cellState: cellState)
        self.handleCelltextColor(view: cell, cellState: cellState)

        self.tableViewController.resetActivities()
        self.tableViewController.downloadActivities(selectedDay: date)
        
        self.dateSelected = date
        self.scrollToHour()
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        self.handleCellSelected(view: cell, cellState: cellState)
        self.handleCelltextColor(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewOfCalendar(from: visibleDates)
    }
}



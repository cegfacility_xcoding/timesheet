//
//  TimeBlocksTableViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 15/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import Firebase

struct BlockView {
    var time: String
    var activity: DownloadedActivity?
    var editMode: Bool = false
    
    init(time: String) {
        self.time = time
    }
}


class TimeBlocksTableViewController: NSObject {
    
    weak var viewcontroller : TimeBlocksViewController!
    
    var selectedUID = ""
    
    var selectedCopyActivity: Attività?
    
    var blocchi : [String] = []
    
    var datasource : [BlockView] = [
        BlockView.init(time: "00:00"),
        BlockView.init(time: "00:30"),
        BlockView.init(time: "01:00"),
        BlockView.init(time: "01:30"),
        BlockView.init(time: "02:00"),
        BlockView.init(time: "02:30"),
        BlockView.init(time: "03:00"),
        BlockView.init(time: "03:30"),
        BlockView.init(time: "04:00"),
        BlockView.init(time: "04:30"),
        BlockView.init(time: "05:00"),
        BlockView.init(time: "05:30"),
        BlockView.init(time: "06:00"),
        BlockView.init(time: "06:30"),
        BlockView.init(time: "07:00"),
        BlockView.init(time: "07:30"),
        BlockView.init(time: "08:00"),
        BlockView.init(time: "08:30"),
        BlockView.init(time: "09:00"),
        BlockView.init(time: "09:30"),
        BlockView.init(time: "10:00"),
        BlockView.init(time: "10:30"),
        BlockView.init(time: "11:00"),
        BlockView.init(time: "11:30"),
        BlockView.init(time: "12:00"),
        BlockView.init(time: "12:30"),
        BlockView.init(time: "13:00"),
        BlockView.init(time: "13:30"),
        BlockView.init(time: "14:00"),
        BlockView.init(time: "14:30"),
        BlockView.init(time: "15:00"),
        BlockView.init(time: "15:30"),
        BlockView.init(time: "16:00"),
        BlockView.init(time: "16:30"),
        BlockView.init(time: "17:00"),
        BlockView.init(time: "17:30"),
        BlockView.init(time: "18:00"),
        BlockView.init(time: "18:30"),
        BlockView.init(time: "19:00"),
        BlockView.init(time: "19:30"),
        BlockView.init(time: "20:00"),
        BlockView.init(time: "20:30"),
        BlockView.init(time: "21:00"),
        BlockView.init(time: "21:30"),
        BlockView.init(time: "22:00"),
        BlockView.init(time: "22:30"),
        BlockView.init(time: "23:00"),
        BlockView.init(time: "23:30")
    ]
    
    var downloadedActivities: [String:DownloadedActivity] = [:]
    
    override init() {
        super.init()
    }
    
    
    func downloadActivities(selectedDay: Date) {
        UserActivity.init(uid: self.selectedUID, date: selectedDay).download() { (block, activity) in
            guard let i = self.datasource.index(where: { (blockView) -> Bool in
                if blockView.time == block { return true }
                return false
            }) else { return }
            
            let hours = Double(self.viewcontroller.hourLabel.text!)
            self.viewcontroller.hourLabel.text = "\(hours! + 0.5)"
            
            
            self.viewcontroller.TimeBlocksTableView.beginUpdates()
            self.datasource[i].activity = activity
            self.viewcontroller.TimeBlocksTableView.reloadRows(at: [IndexPath.init(row: i, section: 0)], with: .left)
            self.viewcontroller.TimeBlocksTableView.endUpdates()
        }
    }
    
    func resetActivities() {
        self.viewcontroller.hourLabel.text = "0"
        for i in 0...self.datasource.count-1 {
            self.datasource[i].activity = nil
        }
        self.viewcontroller.TimeBlocksTableView.reloadData()
    }

 
}


extension TimeBlocksTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TimeBlocksTableViewCell
        cell.reset()
        
        let block = self.datasource[indexPath.row]
        cell.LabelCell.text = block.time
        
        cell.macroLabel.text = block.activity?.macro?.name
        cell.mesoLabel.text = block.activity?.meso?.name
        cell.descriptionTextView.text = block.activity?.note
        
        cell.inoutImage.image = nil

        if let luogo = block.activity?.luogo {
            let image = luogo == "IN" ? #imageLiteral(resourceName: "IN") : #imageLiteral(resourceName: "OUT")
            cell.inoutImage.image = image
        }

        if let macroName = block.activity?.macro?.id {
            let macroColor = MacroColor.array.first(where: {$0.name == macroName})
            cell.barView.backgroundColor = macroColor?.color
        }

        return cell
    }
    
}

extension TimeBlocksTableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
       
        
        guard let activity = self.datasource[indexPath.row].activity else { // guard riga vuota
            
            if self.selectedCopyActivity != nil {
                let paste = UITableViewRowAction.init(style: .normal, title: "Incolla") { (action, indexPath) in
                    let blockView = self.datasource[indexPath.row]
                    self.selectedCopyActivity?.block = blockView.time
                    UserActivity.init(date: self.viewcontroller.dateSelected ?? Date.init()).add(attività: self.selectedCopyActivity!)
                }
                paste.backgroundColor = #colorLiteral(red: 0, green: 0.3058823529, blue: 0.3921568627, alpha: 1)
                
                return [paste]
            }
            
            return []
        }
        
        let edit = UITableViewRowAction.init(style: .normal, title: "Edit") { (action, indexPath) in
            print(indexPath.row)
            var block = self.datasource[indexPath.row]
            block.editMode = true
            self.viewcontroller.performSegue(withIdentifier: "segueToMacro", sender: block)
        }
        
        edit.backgroundColor = #colorLiteral(red: 0, green: 0.3058823529, blue: 0.3921568627, alpha: 1)
        
        let delete = UITableViewRowAction.init(style: .destructive, title: "Delete") { (action, indexPath) in
            guard let activity = self.datasource[indexPath.row].activity else { return }
            self.datasource[indexPath.row].activity = nil
            Database.database().reference().child(activity.ref!).setValue(nil)
            
            let hours = Double(self.viewcontroller.hourLabel.text!)
            self.viewcontroller.hourLabel.text = "\(hours! - 0.5)"
            
            UserActivity.init(date: self.viewcontroller.dateSelected ?? Date.init()).remove(attività: activity)
            
            tableView.reloadRows(at: [indexPath], with: .middle)
        }
        
        let copy = UITableViewRowAction.init(style: .normal, title: "Copy") { (action, indexPath) in
            let blockView = self.datasource[indexPath.row]
            self.selectedCopyActivity = Attività.init(giorno: Date.init(), block: blockView.time, luogo: activity.luogo!, macro: activity.macro!, meso: activity.meso!, note: activity.note!)
        }
        
        copy.backgroundColor = #colorLiteral(red: 0.6352941176, green: 0.8196078431, blue: 0, alpha: 1)
        
        return [delete, edit, copy]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard
            self.selectedUID == CurrentUser.shared.id,
            self.datasource[indexPath.row].activity == nil
        else {
            return
        }

        let block = self.datasource[indexPath.row]
        self.viewcontroller.performSegue(withIdentifier: "segueToMacro", sender: block)
    }
    
}

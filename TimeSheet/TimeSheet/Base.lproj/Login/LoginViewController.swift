//
//  LoginViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 14/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func login_ButtonClicked(_ sender: UIButton) {
    
        guard
            let email = self.emailTextField.text,
            let password = self.passwordTextField.text,
            !email.isEmpty,
            !password.isEmpty
        else {
            let alert = UIAlertController.init(title: "Attenzione", message: "Email o password non inserite", preferredStyle: .alert)
            let closeButton = UIAlertAction.init(title: "Chiudi", style: .destructive, handler: nil)
            alert.addAction(closeButton)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        UserController.init().login(email: email, password: password, completionHandler: { 
            
        }) { 
            let alert = UIAlertController.init(title: "Attenzione", message: "Problemi con il login", preferredStyle: .alert)
            let closeButton = UIAlertAction.init(title: "Chiudi", style: .destructive, handler: nil)
            alert.addAction(closeButton)
            self.present(alert, animated: true, completion: nil)
        }
    
    }
    
    @IBAction func terminiEcondizioni_clicked(_ sender: UIButton) {
        UIApplication.shared.open(URL.init(string: "http://www.cegfacilitysrl.it/")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func passwordDimenticata_clicked(_ sender: Any) {
        UIApplication.shared.open(URL.init(string: "http://www.cegfacilitysrl.it/")!, options: [:], completionHandler: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
    }

}

import UIKit
import Charts

enum ChartType { case macro, meso, inOut }

class BarChartViewController: UIViewController {
    
    @IBOutlet var chartView: BarChartView!
    @IBOutlet var inOutChartView: BarChartView!
    @IBOutlet var mesoChartView: BarChartView!
    
    @IBOutlet var collectionView: UICollectionView!
    
    var collectionController = MonthCollectionViewController.init()
    
    var selectedYear: Int = 0
    var selectedMonth: Int = 0
    
    var selectedChart: ChartType = ChartType.macro
    
    var activities: [String]!
    
    var selectedUID: String = CurrentUser.shared.id
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let components = Calendar.current.dateComponents([.day, .month, .year], from: Date.init())
        self.selectedMonth = components.month!
        self.selectedYear = components.year!
        
        self.collectionController.viewController = self
        self.collectionController.selectedYear = self.selectedYear
        self.collectionView.dataSource = collectionController
        self.collectionView.delegate = collectionController
        
        self.collectionView.scrollToItem(at: IndexPath.init(row: self.selectedMonth - 1, section: 0), at: .centeredHorizontally, animated: true)
        
        DispatchQueue.main.async {
            self.downloadMacro(year: self.selectedYear, month: self.selectedMonth)
        }
        
    }
    
    @IBAction func premonthButton_Clicked(_ sender: UIButton) {
        if self.selectedMonth == 1 {
            self.selectedYear -= 1
            self.collectionController.selectedYear = self.selectedYear
            self.selectedMonth = 12
        } else {
            self.selectedMonth -= 1
        }
    
        let index = IndexPath.init(row: self.selectedMonth - 1, section: 0)
        self.collectionView.selectItem(at: index, animated: true, scrollPosition: UICollectionViewScrollPosition.left)
        self.downloadMacro(year: self.selectedYear, month: self.selectedMonth)
    }
    
    @IBAction func nextmonthButton_Clicked(_ sender: UIButton) {
        if self.selectedMonth == 12 {
            self.selectedYear += 1
            self.collectionController.selectedYear = self.selectedYear
            self.selectedMonth = 1
        } else {
            self.selectedMonth += 1
        }
        
        let index = IndexPath.init(row: self.selectedMonth - 1, section: 0)
        self.collectionView.selectItem(at: index, animated: true, scrollPosition: UICollectionViewScrollPosition.right)
        self.downloadMacro(year: self.selectedYear, month: self.selectedMonth)
    }
    
    @IBAction func macroButton_Clicked(_ sender: UIButton) {
        self.selectedChart = .macro
        self.downloadMacro(year: self.selectedYear, month: selectedMonth)
    }
    
    @IBAction func mesoButton_Clicked(_ sender: UIButton) {
        self.selectedChart = .meso
        self.downloadMeso(year: self.selectedYear, month: self.selectedMonth)
    }
    
    @IBAction func inoutButton_Clicked(_ sender: UIButton) {
        self.selectedChart = .inOut
        self.downloadInOut(year: self.selectedYear, month: selectedMonth)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //download meso
    func downloadMeso(year: Int, month: Int) {
        self.chartView.clear()
        
        ChartsFirebase.init(uid: self.selectedUID).meso(year: year, month: month) { (mesoDict) in
            var bars: [BarChartDataEntry] = []
            
            var max: Double = 0
            var i: Double = 0
            
            for (_, value) in mesoDict {
                let bar = BarChartDataEntry.init(x: i, y: value)
                bars.append(bar)
                
                if (value > max) {
                    max = value
                }
                
                i += 1
            }

            let set = BarChartDataSet.init(values: bars, label: "Ore")
            set.colors = [#colorLiteral(red: 0.5808190107, green: 0.0884276256, blue: 0.3186392188, alpha: 1), #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1), #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1), #colorLiteral(red: 0.5563425422, green: 0.9793455005, blue: 0, alpha: 1), #colorLiteral(red: 0, green: 0.9810667634, blue: 0.5736914277, alpha: 1) ,#colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1), #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1), #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1), #colorLiteral(red: 0.5818830132, green: 0.2156915367, blue: 1, alpha: 1), #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1), #colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 1), #colorLiteral(red: 1, green: 0.4932718873, blue: 0.4739984274, alpha: 1), #colorLiteral(red: 1, green: 0.8323456645, blue: 0.4732058644, alpha: 1) ]
            let data = BarChartData.init(dataSets: [set])
            data.barWidth = 0.9
            
            self.mesoChartView.xAxis.valueFormatter = XValueFormatter.init(dataSource: Array(mesoDict.keys))
            
            self.setChart(type: .meso)
            self.setChartDim(type: .meso, value: Int(max))
            
            self.mesoChartView.isHidden = false
            self.inOutChartView.isHidden = true
            self.chartView.isHidden = true
            self.mesoChartView.data = data
            
            self.mesoChartView.setVisibleXRange(minXRange: 1, maxXRange: 3)
        }
    }
    //Download ufficio fuori
    func downloadInOut(year: Int, month: Int) {
        self.chartView.clear()
        
        ChartsFirebase.init(uid: self.selectedUID).luogo(year: year, month: month) { (inOutDict) in

            var bars: [BarChartDataEntry] = []
            var max: Double = 0
            var i: Double = 0
            var colors3: [UIColor] = []
            
            for (name, value) in inOutDict {
                let bar = BarChartDataEntry.init(x: i, y: value)
                bars.append(bar)
                
                if (value > max) {
                    max = value
                }
                
                if name == "IN" {
                    colors3.append(UIColor.green)
                } else {
                    colors3.append(UIColor.red)
                }
                
                i += 1
            }

            
            let set = BarChartDataSet.init(values: bars, label: "Ore")
            set.colors = colors3
            let data = BarChartData.init(dataSets: [set])
            data.barWidth = 0.9
            
            self.inOutChartView.xAxis.valueFormatter = XValueFormatter.init(dataSource: Array(inOutDict.keys))
            
            self.setChart(type: .inOut)
            self.setChartDim(type: .inOut, value: Int(max))
            
            self.chartView.isHidden = true
            self.mesoChartView.isHidden = true
            self.inOutChartView.isHidden = false
            self.inOutChartView.data = data
            
        }
    }
    
    //download macro
    func downloadMacro(year: Int, month: Int) {
        self.chartView.clear()

        ChartsFirebase.init(uid: self.selectedUID).macro(year: year, month: month, completionHandler: { groups in
            var i: Double = 0
            
            var sets: [BarChartDataSet] = []
            var macroNames: [String] = []
            
            var macroNameTemp: String = ""
            
            var max: Double = 0
            for group in groups {

                var bars: [BarChartDataEntry] = []
                for macro in group.array {
                    
                    let bar = BarChartDataEntry.init(x: i, y: macro.value)
                    macroNames.append(macro.name)
                    macroNameTemp = group.id
                    bars.append(bar)
                    
                    if macro.value > max {
                        max = macro.value
                        self.setChartDim(type: .macro, value: Int(max))
                    }
                    
                    i+=1
                }
                
                let newSet = BarChartDataSet.init(values: bars, label: group.id)
                
                if let color = MacroColor.array.first(where: {$0.name == macroNameTemp})?.color {
                    newSet.colors = [color]
                }

                sets.append(newSet)
                macroNameTemp = ""
            }
            
            let data = BarChartData.init(dataSets: sets)
            data.barWidth = 0.9
            
            self.chartView.xAxis.valueFormatter = XValueFormatter.init(dataSource: macroNames)
            
            self.setChart(type: .macro)
            
            self.chartView.isHidden = false
            self.mesoChartView.isHidden = true
            self.inOutChartView.isHidden = true
            
            self.chartView.data = data
            
            self.chartView.setVisibleXRange(minXRange: 1, maxXRange: 4)
        })
    }
    
    
    func setChart(type: ChartType) {
        
        var chart: BarChartView
        
        switch type {
        case .inOut:
            chart = self.inOutChartView
            
        case .macro:
            chart = self.chartView
            
        case .meso:
            chart = self.mesoChartView
        }
        
        let desc = Description.init()
        desc.text = ""
        
        chart.chartDescription = desc
        chart.xAxis.granularity = 1
        
        chart.leftAxis.valueFormatter = YValueFormatter.init()
        chart.leftAxis.granularity = 0.5
        chart.pinchZoomEnabled = true
        chart.doubleTapToZoomEnabled = false
        chart.rightAxis.enabled = false
        
        chart.xAxis.labelPosition = .bottom
        
        
        chart.xAxis.drawGridLinesEnabled = false
        chart.leftYAxisRenderer.axis?.drawGridLinesEnabled = false
        
        chart.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .easeInBounce)
    }
    
    
    func setChartDim(type: ChartType, value: Int) {
        
        var chart: BarChartView
        
        switch type {
        case .inOut:
            chart = self.inOutChartView
            
        case .macro:
            chart = self.chartView
            
        case .meso:
            chart = self.mesoChartView
        }
        
        chart.leftAxis.axisMaximum = Double(value) + 10
    }
    
    func color3(luogo: String) -> UIColor {
        switch luogo {
        case "IN":
            return UIColor.green
            
        case "OUT":
            return UIColor.red
            
        default: return UIColor.black
        }
    }
    
}


class XValueFormatter: NSObject, IAxisValueFormatter {
    var dataSource: [String] = []
    
    init(dataSource: [String]) {
        super.init()
        self.dataSource = dataSource
    }
    
    //valori sotto barre
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let i = Int(value)
        return dataSource[i]
    }
}

//valori ordinata di sinistra
class YValueFormatter:  NSObject, IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "\(value) h"
    }
}

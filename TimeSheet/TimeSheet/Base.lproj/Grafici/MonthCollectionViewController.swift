//
//  MonthCollectionViewController.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 07/07/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit


class MonthCollectionViewController: NSObject {
    
    var viewController: BarChartViewController! 
    
    var dataSource: [String] = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"]
    
    var selectedYear: Int = 0
    
    func monthNumber(name: String) -> Int {
        return dataSource.index(of: name)! + 1
    }
    
}

extension MonthCollectionViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MonthCollectionViewCell
        
        cell.monthLabel.text = self.dataSource[indexPath.row]
        cell.yearLabel.text = "\(self.selectedYear)"
        return cell
    }

}

extension MonthCollectionViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }

}

extension MonthCollectionViewController: UICollectionViewDelegateFlowLayout {

    // Dimensioni della cella
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIApplication.shared.delegate as! AppDelegate).window!.frame.width
        return CGSize.init(width: width, height: 44)
    }

}

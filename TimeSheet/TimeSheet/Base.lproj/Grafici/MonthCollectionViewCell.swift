//
//  MonthCollectionViewCell.swift
//  TimeSheet
//
//  Created by Giuseppe Sapienza on 07/07/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class MonthCollectionViewCell: UICollectionViewCell {
    @IBOutlet var monthLabel: UILabel! 
    @IBOutlet var yearLabel: UILabel!
}

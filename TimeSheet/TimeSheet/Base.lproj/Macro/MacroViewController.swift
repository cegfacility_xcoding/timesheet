//
//  MacroViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 15/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class MacroViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var macroSearchBar: UISearchBar!
    @IBOutlet weak var macroTableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var blockLabel: UILabel!
    
    var selectedDate: Date?
    var selectedblock: BlockView?
    
    let tableViewController = MacroTableViewController.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.macroSearchBar.delegate = self
        self.tableViewController.viewcontroller = self
        
        self.macroTableView.dataSource = self.tableViewController
        self.macroTableView.delegate = self.tableViewController

        self.dateLabel.text = self.selectedDate?.string
        self.blockLabel.text = self.selectedblock?.time
        
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "segueToMeso":
            let vc = segue.destination as! MesoViewController
            vc.selectedDate = self.selectedDate
            vc.selectedblock = self.selectedblock
            vc.selectedMacro = sender as? Macro

        default: return
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.tableViewController.dataSourceFiltered = self.tableViewController.dataSource
            self.macroTableView.reloadData()
            return
        }
        
        let arrayFiltrato = tableViewController.dataSource.filter { (macro) -> Bool in
            return macro.name.lowercased().hasPrefix(searchText.lowercased())
        }
        
        guard arrayFiltrato.count != 0 else {
            self.tableViewController.dataSourceFiltered = self.tableViewController.dataSource
            self.macroTableView.reloadData()
            return
        }
        
        self.tableViewController.dataSourceFiltered = arrayFiltrato
        self.macroTableView.reloadData()
    }
    
}

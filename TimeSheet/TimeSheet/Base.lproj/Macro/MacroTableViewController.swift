//
//  MacroTableViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 15/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class MacroTableViewController: NSObject {
    weak var viewcontroller : MacroViewController?
    
    var dataSource : [Macro] = []
    var dataSourceFiltered: [Macro] = []
        
    var isFiltered: Bool = false
    
    
    override init() {
        super.init()
        
        MacroController.init().retrieve { (macro) in
            self.dataSource = macro
            self.dataSourceFiltered = macro
            self.viewcontroller?.macroTableView?.reloadData()
        }
    }
    
}

extension MacroTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceFiltered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let macro = self.dataSourceFiltered[indexPath.row]
        cell.textLabel?.text = macro.name
        cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        if let activity = self.viewcontroller?.selectedblock?.activity, activity.macro!.name ==  macro.name {
            cell.backgroundColor = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
        }
        
        return cell
    }
    
}

extension MacroTableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let macro = self.dataSourceFiltered[indexPath.row]
        self.viewcontroller?.performSegue(withIdentifier: "segueToMeso", sender: macro)
    }
    
}

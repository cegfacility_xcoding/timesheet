//
//  NoteViewController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 15/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class NoteViewController: UIViewController {

    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var mesoLabel: UILabel!
    @IBOutlet weak var luogoSegmented: UISegmentedControl!
    
    var selectedmeso : Meso!
    
    var selectedMacro: Macro?
    var selectedDate: Date?
    var selectedblock: BlockView?
    
    var inOut: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        self.mesoLabel.text = self.selectedmeso.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.luogoSegmented.selectedSegmentIndex = -1
        
        guard let activity = self.selectedblock?.activity else { return }
        self.noteTextView.text = activity.note
        self.inOut = activity.luogo
        let index = activity.luogo == "IN" ? 0 : 1
        self.luogoSegmented.selectedSegmentIndex = index
    }

    
    @IBAction func luogoChanged(_ sender: Any) {
        switch luogoSegmented.selectedSegmentIndex{
        case 0:
            self.inOut = "IN"
            
        case 1:
            self.inOut = "OUT"
        default:
            break
        }
     
    }
    
    
    @IBAction func endButton_Clicked(_ sender: UIBarButtonItem) {
        
        guard
            let date = self.selectedDate,
            let blockTime = self.selectedblock?.time,
            let macro = self.selectedMacro,
            let meso = self.selectedmeso
        else { return }
        
        guard
            let _inOut = self.inOut
        else {
            let alert = UIAlertController.init(title: "Attenzione", message: "Seleziona luogo", preferredStyle: .alert)
            let closeButton = UIAlertAction.init(title: "CHIUDI", style: .destructive, handler: nil)
            alert.addAction(closeButton)
            return
        }
        
         let note = self.noteTextView.text ?? ""
            
        let newActivity = Attività.init(giorno: date, block: blockTime, luogo: _inOut, macro: macro, meso: meso, note: note)
        
        if self.selectedblock!.editMode {
            UserActivity.init(date: date).remove(attività: self.selectedblock!.activity!)
        }
        
        UserActivity.init(date: date).add(attività: newActivity)

        self.navigationController?.popToRootViewController(animated: true)

    }

    

}

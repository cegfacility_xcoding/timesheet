//
//  LogoutViewController.swift
//  TimeSheet
//
//  Created by Micol Campoleoni on 19/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LogoutViewController: UIViewController {
    @IBOutlet weak var accountLabel: UILabel!
    
    @IBOutlet weak var assenzaButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let email = Auth.auth().currentUser?.email
        accountLabel.text = email
        
        if(email != "c.dalonzo@cegfacilitysrl.it"){
            assenzaButton.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func logoutButton_Clicked(_ sender: UIButton) {
        
        do {
            
            try Auth.auth().signOut()
            Preferences.shared.accountType = ""

        } catch let error {
            print(error)
            let alert = UIAlertController.init(title: "Errore", message: "Problema con il logout.", preferredStyle: .alert)
            let closeButton = UIAlertAction.init(title: "Chiudi", style: .destructive, handler: nil)
            alert.addAction(closeButton)
            self.present(alert, animated: true, completion: nil)
        }
    }


}

//
//  HomeTabBarController.swift
//  TimeSheet
//
//  Created by Patrick Lucà on 14/06/17.
//  Copyright © 2017 Patrick Lucà. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let block = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Block")
        let logout = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationLogoutViewController")
        let grafici = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationGraficiViewController")

        switch CurrentUser.shared.role {
        case "Admin", "Responsabile":
            let admin = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationAdminViewController")
            self.setViewControllers([block, grafici, admin, logout], animated: true)
            
        default:
            self.setViewControllers([block, grafici, logout], animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
